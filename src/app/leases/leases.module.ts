import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LeasesRoutingModule} from './leases-routing.module';

@NgModule({
  imports: [CommonModule, LeasesRoutingModule]
})
export class LeasesModule {}
