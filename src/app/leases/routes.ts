import {NavigationLink} from '@core/models/navigation-link';

export const leasesRoutes: NavigationLink[] = [
  {
    path: 'units',
    title: 'Business Units',
    children: [
      {
        path: 'list',
        title: 'View Units'
      },
      {
        path: 'create',
        title: 'Create Unit',
        iconClass: 'add'
      }
    ]
  },
  {
    path: 'properties',
    title: 'Properties',
    children: [
      {
        path: 'list',
        title: 'View Properties'
      },
      {
        path: 'create',
        title: 'Create Property',
        iconClass: 'add'
      }
    ]
  },
  {
    path: 'owners',
    title: 'Owners',
    children: [
      {
        path: 'list',
        title: 'View Owners'
      },
      {
        path: 'create',
        title: 'Create Owner',
        iconClass: 'add'
      }
    ]
  },
  {
    path: 'tenants',
    title: 'Tenants',
    children: [
      {
        path: 'list',
        title: 'View Tenants'
      },
      {
        path: 'create',
        title: 'Create Tenant',
        iconClass: 'add'
      }
    ]
  }
];
