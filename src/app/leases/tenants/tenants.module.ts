import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TenantsRoutingModule} from './tenants-routing.module';
import {CoreModule} from '@core/core.module';
import {MaterialModule} from '@material/material.module';
import {TenantsService} from './services/tenants.service';
import {ListComponent} from './components/list/list.component';
import {CreateComponent} from './components/create/create.component';
import {PersonalDetailsComponent} from './components/create/personal-details/personal-details.component';
import {ContactDetailsComponent} from './components/create/contact-details/contact-details.component';
import {AddressDetailsComponent} from './components/create/address-details/address-details.component';
import {AdditionalDetailsComponent} from './components/create/additional-details/additional-details.component';
import {ContractsListComponent} from './components/create/contracts-list/contracts-list.component';
import {NotesListComponent} from './components/create/notes-list/notes-list.component';
import {AdvancedSettingsComponent} from './components/create/advanced-settings/advanced-settings.component';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    PersonalDetailsComponent,
    ContactDetailsComponent,
    AddressDetailsComponent,
    AdditionalDetailsComponent,
    ContractsListComponent,
    NotesListComponent,
    AdvancedSettingsComponent
  ],
  imports: [CommonModule, TenantsRoutingModule, CoreModule, MaterialModule],
  providers: [TenantsService]
})
export class TenantsModule {}
