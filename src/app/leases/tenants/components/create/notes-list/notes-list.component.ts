import {Component, Input} from '@angular/core';
import {Note} from '@leases/owners/models/note';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent {
  @Input() createFlow: boolean = false;

  tableNotesData: Note[] = [];
  tableNotesColumns: string[] = [
    'ID',
    'Tenant',
    'Subject',
    'Type',
    'SubType',
    'Status',
    'ActionDate',
    'Action'
  ];
}
