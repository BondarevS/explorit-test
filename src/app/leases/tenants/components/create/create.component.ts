import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TenantsService} from '../../services/tenants.service';
import {AbstractItemComponent} from '@core/abstract-classes/abstract-item.component';
import {Tenant} from '../../models/tenant';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent extends AbstractItemComponent<Tenant> {
  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public service: TenantsService
  ) {
    super(fb, route, service);
    this.maxStep = 8;
    this.requestActions.get = 'GetTenant';
    this.form = this.fb.group({
      Title: ['', Validators.required],
      Initials: ['', Validators.maxLength(20)],
      Firstname: ['', Validators.maxLength(50)],
      Surname: ['', Validators.maxLength(50)],
      Nickname: ['', Validators.maxLength(20)],
      EmailAddress: ['', [Validators.maxLength(100), Validators.email]],
      MobilePHone: ['', Validators.maxLength(21)],
      HomePhone: ['', Validators.maxLength(21)],
      WorkPhone: ['', Validators.maxLength(21)],
      // Fax: ['', Validators.maxLength(21)],
      PostalAddress: ['', Validators.maxLength(70)],
      ResidentialAddress: ['', Validators.maxLength(70)],
      VATNumber: ['', Validators.maxLength(20)],
      ExternalRef: ['', Validators.maxLength(50)]
    });
  }
}
