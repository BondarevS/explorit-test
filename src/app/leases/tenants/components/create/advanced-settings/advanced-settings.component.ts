import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-advanced-settings',
  templateUrl: './advanced-settings.component.html',
  styleUrls: ['./advanced-settings.component.scss']
})
export class AdvancedSettingsComponent {
  @Input() createFlow: boolean = false;

  debitOrdersData = [];
  debitOrdersColumns: string[] = [
    'AdminID',
    'Tenant',
    'Amount',
    'Status',
    'Tracking',
    'Active',
    'DebitDate',
    'Action'
  ];
}
