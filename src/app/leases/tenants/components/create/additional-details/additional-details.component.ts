import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-additional-details',
  templateUrl: './additional-details.component.html',
  styleUrls: ['./additional-details.component.scss']
})
export class AdditionalDetailsComponent extends AbstractItemPanelComponent {}
