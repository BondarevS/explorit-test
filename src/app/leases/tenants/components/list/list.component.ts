import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {TenantsService} from '../../services/tenants.service';
import {AbstractBaseListComponent} from '@core/abstract-classes/abstract-base-list.component';
import {Tenant} from '../../models/tenant';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends AbstractBaseListComponent<Tenant, any> {
  constructor(public fb: FormBuilder, public service: TenantsService) {
    super(fb, service);
    this.tableFilter.field = 'AdminID';
    this.tableColumns = [
      'AdminID',
      'Surname',
      'Firstname',
      'EmailAddress',
      'MobilePHone',
      'property',
      'propertyNumber',
      'propertyType',
      'Action'
    ];
  }
}
