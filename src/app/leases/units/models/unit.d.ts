export interface Unit {
  ID: number;
  AdminID: string;
  Reference: string;
  Name: string;
  Type: string;
}
