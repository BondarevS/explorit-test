import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {UnitsRoutingModule} from './units-routing.module';
import {CoreModule} from '@core/core.module';
import {MaterialModule} from '@material/material.module';
import {UnitsService} from './services/units.service';
import {ListComponent} from './components/list/list.component';
import {CreateComponent} from './components/create/create.component';
import {UnitDetailsComponent} from './components/create/unit-details/unit-details.component';
import {AccountingDetailsComponent} from './components/create/accounting-details/accounting-details.component';
import {IntegrationsDetailsComponent} from './components/create/integrations-details/integrations-details.component';
import {AdvancedDetailsComponent} from './components/create/advanced-details/advanced-details.component';
import {StationaryDetailsComponent} from './components/create/stationary-details/stationary-details.component';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    UnitDetailsComponent,
    AccountingDetailsComponent,
    IntegrationsDetailsComponent,
    AdvancedDetailsComponent,
    StationaryDetailsComponent
  ],
  imports: [
    CommonModule,
    UnitsRoutingModule,
    CoreModule,
    MaterialModule,
    AngularEditorModule,
    NgxDropzoneModule
  ],
  providers: [UnitsService]
})
export class UnitsModule {}
