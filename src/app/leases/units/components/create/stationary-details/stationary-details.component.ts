import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-stationary-details',
  templateUrl: './stationary-details.component.html',
  styleUrls: ['./stationary-details.component.scss']
})
export class StationaryDetailsComponent extends AbstractItemPanelComponent {
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [['bold']]
  };

  reportHeaderImg: File | null = null;
  reportFooterImg: File | null = null;

  onSelect(event: any, header: boolean = true) {
    if (header) {
      this.reportHeaderImg = event.addedFiles[0];
    } else {
      this.reportFooterImg = event.addedFiles[0];
    }
  }

  onRemoveHeader() {
    this.reportHeaderImg = null;
  }

  onRemoveFooter() {
    this.reportFooterImg = null;
  }
}
