import {ComponentFixture, TestBed} from '@angular/core/testing';
import {StationaryDetailsComponent} from './stationary-details.component';

describe('StationaryDetailsComponent', () => {
  let component: StationaryDetailsComponent;
  let fixture: ComponentFixture<StationaryDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StationaryDetailsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StationaryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
