import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-advanced-details',
  templateUrl: './advanced-details.component.html',
  styleUrls: ['./advanced-details.component.scss']
})
export class AdvancedDetailsComponent extends AbstractItemPanelComponent {}
