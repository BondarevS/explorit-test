import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-accounting-details',
  templateUrl: './accounting-details.component.html',
  styleUrls: ['./accounting-details.component.scss']
})
export class AccountingDetailsComponent extends AbstractItemPanelComponent {}
