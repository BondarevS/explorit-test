import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-integrations-details',
  templateUrl: './integrations-details.component.html',
  styleUrls: ['./integrations-details.component.scss']
})
export class IntegrationsDetailsComponent extends AbstractItemPanelComponent {}
