import {ComponentFixture, TestBed} from '@angular/core/testing';
import {IntegrationsDetailsComponent} from './integrations-details.component';

describe('IntegrationsDetailsComponent', () => {
  let component: IntegrationsDetailsComponent;
  let fixture: ComponentFixture<IntegrationsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IntegrationsDetailsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
