import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {UnitsService} from '../../services/units.service';
import {AbstractItemComponent} from '@core/abstract-classes/abstract-item.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent extends AbstractItemComponent<any> {
  constructor(public fb: FormBuilder, public route: ActivatedRoute, public service: UnitsService) {
    super(fb, route, service);
    this.maxStep = 4;
    this.form = this.fb.group({
      ReportFooterText: '',
      OwnReportFooterText: '',
      TenReportFooterText: ''
    });
  }
}
