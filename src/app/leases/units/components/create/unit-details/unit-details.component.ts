import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-unit-details',
  templateUrl: './unit-details.component.html',
  styleUrls: ['./unit-details.component.scss']
})
export class UnitDetailsComponent extends AbstractItemPanelComponent {}
