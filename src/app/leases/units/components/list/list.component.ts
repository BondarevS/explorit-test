import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AbstractBaseListComponent} from '@core/abstract-classes/abstract-base-list.component';
import {UnitsService} from '../../services/units.service';
import {Unit} from '../../models/unit';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends AbstractBaseListComponent<Unit, any> {
  constructor(public fb: FormBuilder, public service: UnitsService) {
    super(fb, service);
    this.tableFilter.field = 'AdminID';
    this.tableColumns = ['AdminID', 'Reference', 'Name', 'Type', 'Action'];
  }
}
