import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PropertiesRoutingModule} from './properties-routing.module';
import {CoreModule} from '@core/core.module';
import {MaterialModule} from '@material/material.module';
import {ListComponent} from './components/list/list.component';
import {CreateComponent} from './components/create/create.component';
import {PropertiesService} from './services/properties.service';
import {PropertyDetailsComponent} from './components/create/property-details/property-details.component';
import {PropertyLocationComponent} from './components/create/property-location/property-location.component';
import {ListingDetailsComponent} from './components/create/listing-details/listing-details.component';
import {ListingFeaturesComponent} from './components/create/listing-features/listing-features.component';
import {ListingAgentComponent} from './components/create/listing-agent/listing-agent.component';
import {ListingPublishingComponent} from './components/create/listing-publishing/listing-publishing.component';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    PropertyDetailsComponent,
    PropertyLocationComponent,
    ListingDetailsComponent,
    ListingFeaturesComponent,
    ListingAgentComponent,
    ListingPublishingComponent
  ],
  imports: [CommonModule, PropertiesRoutingModule, CoreModule, MaterialModule],
  providers: [PropertiesService]
})
export class PropertiesModule {}
