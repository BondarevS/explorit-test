import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AbstractBaseListComponent} from '@core/abstract-classes/abstract-base-list.component';
import {PropertiesService} from '../../services/properties.service';
import {Property} from '../../models/property';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends AbstractBaseListComponent<Property, any> {
  constructor(public fb: FormBuilder, public service: PropertiesService) {
    super(fb, service);
    this.tableFilter.field = 'AdminID';
    this.tableColumns = [
      'AdminID',
      'Unit',
      'ShortDescription',
      'Description',
      'OriginalReference',
      'PROPADDRESS_SUBURB',
      'Action'
    ];
  }
}
