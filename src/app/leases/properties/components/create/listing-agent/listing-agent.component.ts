import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-listing-agent',
  templateUrl: './listing-agent.component.html',
  styleUrls: ['./listing-agent.component.scss']
})
export class ListingAgentComponent extends AbstractItemPanelComponent {}
