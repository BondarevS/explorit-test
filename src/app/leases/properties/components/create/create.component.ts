import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {PropertiesService} from '../../services/properties.service';
import {AbstractItemComponent} from '@core/abstract-classes/abstract-item.component';
import {Property} from '../../models/property';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent extends AbstractItemComponent<Property> {
  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public service: PropertiesService
  ) {
    super(fb, route, service);
    this.maxStep = 5;
    this.requestActions.get = 'GetProperty';
    this.form = this.fb.group({
      AdminID: ['', [Validators.maxLength(10), Validators.required]],
      ListingStatus: ['', Validators.maxLength(50)],
      Type: ['', Validators.maxLength(30)],
      ErfNumber: ['', Validators.maxLength(20)],
      PROPADDRESS_STREET: ['', Validators.maxLength(70)],
      BUILDING: ['', Validators.maxLength(100)],
      UNITNUMBER: ['', Validators.maxLength(50)],
      PROPADDRESS_BUILDING: ['', Validators.maxLength(70)],
      PROPADDRESS_SUBURB: ['', Validators.maxLength(70)],
      PROPADDRESS_TOWN: ['', Validators.maxLength(70)],
      PROPADDRESS_PROVINCE: ['', Validators.maxLength(70)],
      PROPADDRESS_CODE: ['', Validators.maxLength(4)],
      BEDROOM: '',
      BATHROOM: '',
      TOILETS: '',
      STUDY: '',
      STOREROOM: '',
      KITCHEN: '',
      ENCLOSEDGARAGE: '',
      ANCILLARYQ: '',
      ANCILLARYT: '',
      OriginalReference: ['', Validators.maxLength(20)],
      ListingDepositRequired: '',
      Valuation: '',
      ListingRental: '',
      ENTRANCEHALL: '',
      FAMILYROOM: '',
      DININGROOM: '',
      SECURITY: false,
      POOL: false,
      LOUNGE: false,
      PROCARPORT: false,
      TVANT: false,
      COURTYARD: false,
      LAUNDRY: false,
      PETSALLOWED: false,
      GARDEN: false,
      PartiallyFenced: false,
      PATIO: false,
      TotalyFenced: false,
      VERANDA: false,
      ListingAgent: ['', Validators.maxLength(40)],
      AgentEMail: ['', [Validators.maxLength(40), Validators.email]],
      AgentPhone1: ['', Validators.maxLength(15)],
      AgentPhone2: ['', Validators.maxLength(15)],
      AgentSMSNumber: ['', Validators.maxLength(10)],
      DATEAVAILABLE: '',
      ShortDescription: ['', Validators.maxLength(24)],
      Description: ['', Validators.maxLength(50)],
      DetailedDescription: ['', Validators.maxLength(512)],
      ListingDepositComments: ['', Validators.maxLength(100)]
    });
  }
}
