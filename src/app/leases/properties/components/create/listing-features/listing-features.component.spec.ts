import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ListingFeaturesComponent} from './listing-features.component';

describe('ListingFeaturesComponent', () => {
  let component: ListingFeaturesComponent;
  let fixture: ComponentFixture<ListingFeaturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListingFeaturesComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
