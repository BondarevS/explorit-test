import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-listing-features',
  templateUrl: './listing-features.component.html',
  styleUrls: ['./listing-features.component.scss']
})
export class ListingFeaturesComponent extends AbstractItemPanelComponent {}
