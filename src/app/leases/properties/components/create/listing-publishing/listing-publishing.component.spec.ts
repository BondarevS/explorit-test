import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ListingPublishingComponent} from './listing-publishing.component';

describe('ListingPublishingComponent', () => {
  let component: ListingPublishingComponent;
  let fixture: ComponentFixture<ListingPublishingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListingPublishingComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingPublishingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
