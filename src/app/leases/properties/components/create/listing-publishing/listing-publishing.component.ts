import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-listing-publishing',
  templateUrl: './listing-publishing.component.html',
  styleUrls: ['./listing-publishing.component.scss']
})
export class ListingPublishingComponent extends AbstractItemPanelComponent {}
