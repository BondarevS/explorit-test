import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ModuleLayoutComponent} from '@core/components/module-layout/module-layout.component';
import {leasesRoutes} from './routes';

const routes: Routes = [
  {
    path: '',
    component: ModuleLayoutComponent,
    data: {links: leasesRoutes, showWizard: true},
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'units'
      },
      {
        path: 'units',
        loadChildren: () => import('./units/units.module').then((m) => m.UnitsModule)
      },
      {
        path: 'properties',
        loadChildren: () => import('./properties/properties.module').then((m) => m.PropertiesModule)
      },
      {
        path: 'owners',
        loadChildren: () => import('./owners/owners.module').then((m) => m.OwnersModule)
      },
      {
        path: 'tenants',
        loadChildren: () => import('./tenants/tenants.module').then((m) => m.TenantsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeasesRoutingModule {}
