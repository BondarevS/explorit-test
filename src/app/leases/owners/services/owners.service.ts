import {Injectable, Injector} from '@angular/core';
import {AbstractApiService} from '@core/abstract-classes/abstract-api-service';

@Injectable()
export class OwnersService extends AbstractApiService {
  constructor(injector: Injector) {
    super(injector, 'psapi/owners.aspx', 'owners');
  }
}
