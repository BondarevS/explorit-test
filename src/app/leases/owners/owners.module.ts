import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OwnersRoutingModule} from './owners-routing.module';
import {MaterialModule} from '@material/material.module';
import {CoreModule} from '@core/core.module';
import {ListComponent} from './components/list/list.component';
import {CreateComponent} from './components/create/create.component';
import {OwnersService} from './services/owners.service';
import {PersonalDetailsComponent} from './components/create/personal-details/personal-details.component';
import {ContactDetailsComponent} from './components/create/contact-details/contact-details.component';
import {AddressDetailsComponent} from './components/create/address-details/address-details.component';
import {NotesListComponent} from './components/create/notes-list/notes-list.component';
import {OwnerSettingsComponent} from './components/create/owner-settings/owner-settings.component';
import {ContractsListComponent} from './components/create/contracts-list/contracts-list.component';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    PersonalDetailsComponent,
    ContactDetailsComponent,
    AddressDetailsComponent,
    NotesListComponent,
    OwnerSettingsComponent,
    ContractsListComponent
  ],
  imports: [CommonModule, OwnersRoutingModule, CoreModule, MaterialModule],
  providers: [OwnersService]
})
export class OwnersModule {}
