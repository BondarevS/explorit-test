import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AbstractBaseListComponent} from '@core/abstract-classes/abstract-base-list.component';
import {Owner} from '../../models/owner';
import {OwnersService} from '../../services/owners.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends AbstractBaseListComponent<Owner, any> {
  constructor(public fb: FormBuilder, public service: OwnersService) {
    super(fb, service);
    this.tableFilter.action = 'getAllOwners';
    this.tableFilter.field = 'Surname';
    this.tableColumns = [
      'AdminID',
      'Surname',
      'Firstname',
      'EmailAddress',
      'MobilePhone',
      'property',
      'propertyNumber',
      'propertyType',
      'Action'
    ];

    // ToDo next line make request to API
    // this.requestOption.lifeRequest = true;
  }
}
