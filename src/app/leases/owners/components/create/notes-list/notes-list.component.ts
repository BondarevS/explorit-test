import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';
import {Note} from '../../../models/note';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent extends AbstractItemPanelComponent {
  tableNotesData: Note[] = [];
  tableNotesColumns: string[] = [
    'ID',
    'Tenant',
    'Subject',
    'Type',
    'SubType',
    'Status',
    'ActionDate',
    'Action'
  ];
}
