import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-owner-settings',
  templateUrl: './owner-settings.component.html',
  styleUrls: ['./owner-settings.component.scss']
})
export class OwnerSettingsComponent extends AbstractItemPanelComponent {
  debitOrdersData = [];
  debitOrdersColumns: string[] = [
    'AdminID',
    'Tenant',
    'Amount',
    'Status',
    'Tracking',
    'Active',
    'DebitDate',
    'Action'
  ];

  agentCommissionData = [];
  agentCommissionColumns: string[] = ['Number', 'Agent', 'Percent', 'Status'];
}
