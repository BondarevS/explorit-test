import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent extends AbstractItemPanelComponent {}
