import {Component} from '@angular/core';
import {AbstractItemPanelComponent} from '@core/abstract-classes/abstract-item-panel.component';

@Component({
  selector: 'app-contracts-list',
  templateUrl: './contracts-list.component.html',
  styleUrls: ['./contracts-list.component.scss']
})
export class ContractsListComponent extends AbstractItemPanelComponent {}
