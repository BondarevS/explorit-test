export interface OwnerContract {
  ngStatus: string; // basic status after update - possible values ['OK','CHANGED','UPDATED','FAIL','ERROR']
  ngStatusMessage: string; // user-friendly descripiton of ngStatus
  ID: number; // int
  OWNAdminID: string; // varchar(10) - fk to Owner AdminID (OWNxxxxx)
  PROAmdinID: string; // varchar(10)) - fk to Property AdminID (PROxxxxx)
  MandateStartDate?: string; // date - start date of Mandate with Agency (optional)
  MandateEndDate?: string; // date - end date of Mandate with Agency (optional)
  MandateInstruction?: string; // ntext - notes/instructions (optional)
  Rent: number; // money - current Monthly Rental amount
  EscalationDate?: string; // date - next Escalation Date (optional)
  EscalationPercentage?: number; // float - Escalation Percentage (optional)
  AgencyFee: number; // money - Estate Agent's Fee
  SubAGENCYAcc?: string; // varchar(10) - deprecated - fk to GLAccount (ACCxxxxx) (optional)
  InvoiceAgencyFeeOnReceipt?: boolean; // bit - deprecated - marked to invoice an Agency Fee on each Rental Receipt (optional)
  Terminated: boolean; // bit - Terminated Contract denotes inactive/unmanaged Properties
  InvoiceForRENT?: boolean; // bit - when marked, system will create an Invoice for each Rental Charge (optional)
  InvoiceForRentVAT?: boolean; // bit - when marked, system will process VAT on the Rental amount (not for Residential Units) (optional)
  StartDate?: string; // date - when the actual start date differs from Mandate Start Date (optional)
  EndDate?: string; // date - when the actual end date differs from the Mandate End Date (optional)
  BankName?: string; // varchar(20) - Name of Bank for payments to Owner (optional)
  BranchName?: string; // varchar(20) - Name of Bank Branch for payments to Owner (optional)
  BranchCode?: string; // varchar(20) - Branch Code for Owner Payments (optional)
  BankAccountNumber?: string; // varchar(20) - Bank Account NR for Owner Payments (optional)
  BankAccountHolder?: string; // varchar(50) - Bank Account Holder (optional)
  BankAccountType?: string; // varchar(20) - Bank Account Type {'SAVINGS','CHEQUE','BOND'} (optional)
  OwnerReference?: string; // varchar(20) - Reference for Owner Bank Statement (BTRANSFERCODE) (optional)
  AgencyReference?: string; // varchar(20) - Reference for Agency Bank Statement (TRANSRemitBankRef) (optional)
  PrintedStatement?: boolean; // bit - Owner prefers to receive a printed statement (optional)
  CompanyID: number; // int - Company ID
}
