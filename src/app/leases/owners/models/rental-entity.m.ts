export interface RentalEntity {
  ngStatus: string;
  ngStatusMessage: string;
  ID: number; // int Unique_ID (PK)
  AdminID: string; // varchar(10) Owner AdminID, Property AdminID or such (PROxxxxx, OWNxxxxx)
  Name: string; // Name to identify or refer to
}
