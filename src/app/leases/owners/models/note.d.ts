export interface Note {
  ngStatus: string; // basic status after update - possible values ['OK','CHANGED','UPDATED','FAIL','ERROR']
  ngStatusMessage: string; // user-friendly descripiton of ngStatus
  ID: number; // int - PK
  ParentID: number; // int - fk to own PK - should a note be linked/child to another note
  PROAdminID: string; // varchar(10) - fk to Property AdminID (PROxxxxx)
  OWNAdminID: string; // varchar(10) - fk to Owner AdminID (OWNxxxxx)
  TENAdminID: string; // varchar(10) - fk to Tenant AdminID (TENxxxxx)
  OWNView: boolean; // bit - does Owner have access to this note
  TENView: boolean; // bit - does Tenant have access to this note
  Subject: string; // varchar(100) - Subject of this note
  Text: string; // ntext - Continuous text of notes made - main text
  Type: string; // varchar(20) - Note main category {'USER','MAINTENANCE','CREDIT CONTROL'}
  SubType: string; // varchar(20)) - Note sub category {'GENERAL','INSPECTION','FICA','REPAIR','LEGAL','ACCOUNT QUERY','PROOF OF PAYMENT'}
  ActionDate: string; // date - Note Completion date or Reminder date
  Status: string; // varchar(20) - Note Status {'TO DO','DONE','ARCHIVED'}
  CompanyID: number; // int - Company ID
}
