import {RentalEntity} from './rental-entity.m';

export interface Owner {
  // Represents an Owner - also known as Landlord - that owns Properties which are Rented/Managed
  ngStatus: string; // basic status after update - possible values ['OK','CHANGED','UPDATED','FAIL','ERROR']
  ngStatusMessage: string; // user-friendly description of ngStatus
  ID: number;
  AdminID: string; // nvarchar(10) alphanumeric company-wide unique reference like OWN00001 for the first Owner
  Surname?: string; // nvarchar(50) (optional)
  Firstname?: string; // nvarchar(50) (optional)
  Initials?: string; // nvarchar(20) (optional)
  Title?: string; // nvarchar(10) (optional)
  Nickname?: string; // nvarchar(20) (optional)
  PostalAddress?: string; // nvarchar(70) (optional)
  ResidentialAddress?: string; // nvarchar(70) (optional)
  EmailAddress?: string; // nvarchar(100) (optional)
  WorkPhone?: string; // varchar(21) (optional)
  HomePhone?: string; // varchar(21) (optional)
  MobilePhone?: string; // varchar(21) (optional)
  Fax?: string; // varchar(21) (optional)
  Notes?: string; // varchar(20) (optional)
  IDNumber?: string; // varchar(15) - National Identity Number or Passport Number (optional)
  VATNumber?: string; // nvarchar(20) (optional)
  OldRef?: string; // nvarchar(50) - reference on a previous system (optional)
  ExternalRef?: string; // nvarchar(50) - reference on external system (optional)
  CompanyID: number; // int - Company ID

  Properties?: RentalEntity;
}
