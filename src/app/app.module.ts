import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxSpinnerModule} from 'ngx-spinner';
import {RequestInterceptor} from '@core/interceptors/request.interceptor';
import {AppComponent} from './app.component';
import {HeaderModule} from './shared-modules/header/header.module';
import {SidebarModule} from './shared-modules/sidebar/sidebar.module';
import {AppLayoutComponent} from '@core/components/app-layout/app-layout.component';
import {ModuleLayoutComponent} from '@core/components/module-layout/module-layout.component';
import {MAT_DATE_FORMATS} from '@angular/material/core';

@NgModule({
  declarations: [AppComponent, AppLayoutComponent, ModuleLayoutComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule,
    HeaderModule,
    SidebarModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'MM/DD/YYYY'
        },
        display: {
          dateInput: 'MM/DD/YYYY',
          monthYearLabel: 'MMMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY'
        }
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
