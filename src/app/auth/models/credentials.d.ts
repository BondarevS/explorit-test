export interface Credentials {
  actions: string;
  sUsername: string;
  sPassword: string;
}
