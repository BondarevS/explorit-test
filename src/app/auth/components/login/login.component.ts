import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {AuthService} from '../../services/auth.service';
import {ProfileService} from '../../../shared-modules/header/services/profile.service';
import {RequestOptions} from '@core/models/request-options';
import {User} from '../../../shared-modules/header/models/user';

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup = this.fb.group({
    sUserName: ['', Validators.required],
    sPassword: ['', Validators.required]
  });

  showLoginError = false;

  errorText = 'Invalid username or password';

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private profileService: ProfileService
  ) {}

  submit(): void {
    if (this.form.invalid) {
      return this.form.markAllAsTouched();
    }

    const sendData: Credential = {
      action: 'ngNewLogin',
      ...this.form.value
    };
    const sendOpt: RequestOptions = {
      lifeRequest: true,
      showSpinner: true
    };

    this.authService
      .reqPost<User>(sendData, sendOpt)
      .pipe(untilDestroyed(this))
      .subscribe(
        (res) => {
          this.profileService.$profile.next(res);
          // this.router.navigate(['/']);
        },
        (error) => {
          if (error.status === 401) {
            this.showLoginError = true;
          }
        }
      );
  }
}
