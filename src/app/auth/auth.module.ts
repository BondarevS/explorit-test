import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@material/material.module';
import {CoreModule} from '@core/core.module';
import {AuthRoutingModule} from './auth-routing.module';
import {AuthService} from './services/auth.service';
import {AuthComponent} from './auth.component';
import {LoginComponent} from './components/login/login.component';

@NgModule({
  declarations: [LoginComponent, AuthComponent],
  imports: [CommonModule, AuthRoutingModule, MaterialModule, CoreModule],
  providers: [AuthService]
})
export class AuthModule {}
