import {Pipe, PipeTransform} from '@angular/core';
import {MeterTypes} from '@core/enums/meterTypes.enum';

@Pipe({
  name: 'meterType'
})
export class MeterTypePipe implements PipeTransform {
  transform(value: number): string {
    switch (value) {
      case MeterTypes.WATER:
        return 'Water';
      case MeterTypes.ELECTRICITY:
        return 'Electricity';
      case MeterTypes.CAS:
        return 'Gas';
      case MeterTypes.SANITATION:
        return 'Sanitation';
      default:
        return '';
    }
  }
}
