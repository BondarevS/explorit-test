import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CoreService} from '../services/core.service';
import {ProfileService} from '../../shared-modules/header/services/profile.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private profileService: ProfileService, private coreService: CoreService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.coreService.isAuthenticated()) {
      request = request.clone({
        headers: request.headers.set('Authorization', this.coreService.getToken())
      });
    }

    if (this.profileService.$profile.getValue()) {
      const user = this.profileService.$profile.getValue();

      request = request.clone({
        body: {...(request?.body as object), icmpyid: user?.CompanyID}
      });
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => this.handleAuthError(error))
    );
  }

  private handleAuthError(error: HttpErrorResponse): Observable<any> {
    if (error.status === 401) {
      this.coreService.logout();
    }

    return throwError(error);
  }
}
