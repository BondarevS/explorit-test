export interface NavigationLink {
  path: string;
  title: string;
  iconClass?: string;
  children?: NavigationLink[];
}
