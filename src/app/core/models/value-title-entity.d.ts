export interface ValueTitleEntity {
  value: string | number;
  title: string;
}
