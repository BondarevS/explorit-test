export interface RequestOptions {
  showSpinner?: boolean;
  lifeRequest?: boolean;
  fileName?: string;
}
