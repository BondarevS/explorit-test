import {SortDirection} from '@angular/material/sort';

export interface ApiListFilter {
  action: string;
  page: number;
  perPage: number;
  field?: string;
  direction?: SortDirection;
}
