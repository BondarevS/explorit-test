import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BackButtonComponent} from './components/back-button/back-button.component';
import {ValidateMessageComponent} from './components/validate-message/validate-message.component';
import {MeterTypePipe} from '@core/pipes/meter-type.pipe';

@NgModule({
  declarations: [BackButtonComponent, ValidateMessageComponent, MeterTypePipe],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, HttpClientModule],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BackButtonComponent,
    ValidateMessageComponent,
    MeterTypePipe
  ]
})
export class CoreModule {}
