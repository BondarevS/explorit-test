import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {AbstractList} from '@core/abstract-classes/abstract-list';
import {AbstractApiService} from '@core/abstract-classes/abstract-api-service';
import {ApiListResponse} from '@core/models/api-list-response';

@UntilDestroy()
@Component({template: ''})
export abstract class AbstractBaseListComponent<T, N> extends AbstractList<T, N> implements OnInit {
  protected constructor(public fb: FormBuilder, public service: AbstractApiService) {
    super(fb, service);
  }

  ngOnInit(): void {
    this.fetchList();
    this.filterList();
  }

  protected fetchList(): void {
    this.service
      .reqPost<ApiListResponse<T>>(this.tableFilter, this.requestOption)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.tableData = res.data;
        this.total = res.meta.total;
      });
  }
}
