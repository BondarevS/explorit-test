import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Params} from '@angular/router';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {AbstractApiService} from './abstract-api-service';
import {MatAccordion} from '@angular/material/expansion';
import {RequestOptions} from '../models/request-options';
import {ApiResponse} from '@core/models/api-response';

@UntilDestroy()
@Component({template: ''})
export abstract class AbstractItemComponent<T> implements OnInit {
  @ViewChild('accordion', {static: true}) Accordion!: MatAccordion;

  form!: FormGroup;

  requestActions: {[key: string]: string} = {
    get: '',
    create: '',
    patch: ''
  };

  requestOption: RequestOptions = {
    lifeRequest: false,
    showSpinner: true,
    fileName: 'item.json'
  };

  entity!: T;

  createFlow = true;

  // Expanded accordion
  openedAll = false;
  step: number = 0;
  maxStep: number = 1;

  protected constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public service: AbstractApiService
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(untilDestroyed(this)).subscribe((params: Params) => {
      if (params.id) {
        this.createFlow = false;
        this.fetchEntity(params.id);
      }
    });
  }

  protected fetchEntity(adminID: string): void {
    this.service
      .reqPost<ApiResponse<T>>({action: this.requestActions.get, AdminID: adminID}, this.requestOption)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.entity = res.data;
        this.form.patchValue(this.entity);
        this.form.disable();
      });
  }

  public toggleEditMode(): void {
    if (this.form.disabled) {
      this.form.enable();
    } else {
      this.form.disable();
    }
  }

  public submit(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.entity ? this.update() : this.create();
  }

  protected create(): void {
    this.service
      .reqPost<T>({action: this.requestActions.patch, ...this.form.value}, this.requestOption)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.entity = res;
        this.form.patchValue(this.entity);
        this.nextStep();
      });
  }

  protected update(): void {
    this.form.disable();
    if (this.maxStep >= this.step + 1) {
      this.nextStep();
    }
    return;
    this.service
      .reqPatch<T>(
        {
          action: this.requestActions.patch,
          // @ts-ignore
          AdminID: this.entity?.AdminID,
          ...this.form.value
        },
        this.requestOption
      )
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.entity = res.data;
        this.form.patchValue(this.entity);
        this.form.disable();
        this.nextStep();
      });
  }

  public nextStep() {
    this.step++;
  }

  public setStep(index: number): void {
    this.step = index;
  }

  public prevStep(): void {
    this.step--;
  }

  public togglePanels(): void {
    this.openedAll ? this.Accordion.closeAll() : this.Accordion.openAll();
    this.openedAll = !this.openedAll;
  }
}
