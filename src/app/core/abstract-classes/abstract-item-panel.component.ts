import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({template: ''})
export abstract class AbstractItemPanelComponent {
  @Input() form!: FormGroup;
  @Input() createFlow = true;
  @Input() editMode = false;
  @Input() entity!: any;

  @Output() submitEvent = new EventEmitter<void>();
  @Output() toggleEditModeEvent = new EventEmitter<void>();
  @Output() prevStepEvent = new EventEmitter<void>();
}
