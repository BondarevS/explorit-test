import {FormBuilder, FormGroup} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Sort} from '@angular/material/sort/sort';
import {SortDirection} from '@angular/material/sort';
import {PageEvent} from '@angular/material/paginator/public-api';
import {ApiListFilter} from '../models/api-list-filter';
import {AbstractApiService} from './abstract-api-service';
import {RequestOptions} from '../models/request-options';
import {debounceTime} from 'rxjs/operators';

@UntilDestroy()
export abstract class AbstractList<T, N> {
  form: FormGroup = this.fb.group({
    search: ''
  });

  requestOption: RequestOptions = {
    lifeRequest: false,
    showSpinner: true,
    fileName: 'list.json'
  };

  tableData: T[] = [];

  tableFilter: ApiListFilter | N = {
    action: '',
    page: 1,
    perPage: 10,
    direction: 'desc'
  };

  tableColumns: string[] = [];

  total = 0;

  protected constructor(public fb: FormBuilder, public service: AbstractApiService) {}

  protected filterList(): void {
    this.form.valueChanges.pipe(debounceTime(1000), untilDestroyed(this)).subscribe((value) => {
      this.tableFilter = {...this.tableFilter, ...value, page: 1};
      this.fetchList();
    });
  }

  sortTable(event: Sort): void {
    this.tableFilter = {
      ...this.tableFilter,
      field: event.active,
      direction: event.direction.toLowerCase() as SortDirection
    };
    this.fetchList();
  }

  onPageChange(event: PageEvent): void {
    this.tableFilter = {
      ...this.tableFilter,
      page: event.pageIndex + 1,
      perPage: event.pageSize
    };
    this.fetchList();
  }

  protected fetchList(): void {}
}
