import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Injector} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {environment} from '@env/environment';
import {ApiResponse} from '../models/api-response';
import {RequestOptions} from '../models/request-options';

export abstract class AbstractApiService {
  protected http: HttpClient;
  protected spinner: NgxSpinnerService;

  public baseUrl: string = environment.baseUrl;
  public baseMockUrl: string = environment.baseMockUrl;

  protected constructor(
    injector: Injector,
    public prefix: string = '',
    public mockPrefix: string = ''
  ) {
    this.http = injector.get(HttpClient);
    this.spinner = injector.get(NgxSpinnerService);
  }

  public getUrl(options: RequestOptions): string {
    if (options?.lifeRequest) {
      return `${this.baseUrl}/${this.prefix}`;
    }
    return `${this.baseMockUrl}/${this.mockPrefix}/${options.fileName}`;
  }

  public request<T>(req: Observable<T>, options: RequestOptions = {}): Observable<T> {
    if (options.showSpinner) {
      this.spinner.show();
    }

    return req.pipe(
      finalize(() => {
        if (options.showSpinner) {
          this.spinner.hide();
        }
      }),
      catchError((err) => {
        if (options.showSpinner) {
          this.spinner.hide();
        }
        return throwError(err);
      })
    );
  }

  public reqPatch<T>(body: object, options: RequestOptions = {}): Observable<ApiResponse<T>> {
    return this.request(
      this.http.patch<ApiResponse<T>>(this.getUrl(options), body, {
        withCredentials: true
      }),
      options
    )
  }

  public reqPost<T>(body: object | any, options: RequestOptions = {}): Observable<T> {
    const lRequest = options?.lifeRequest
      ? this.http.post<T>(this.getUrl(options), body, {withCredentials: true})
      : this.http.get<T>(this.getUrl(options));

    return this.request(lRequest, options);
  }

  public reqGetFile(params: {}, options: RequestOptions = {}): Observable<HttpResponse<Blob>> {
    return this.request(
      this.http.get(this.getUrl(options), {
        params,
        responseType: 'blob',
        withCredentials: true,
        observe: 'response'
      }),
      options
    );
  }

  public downloadFile(response: Blob, filename: string): void {
    const downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(response);
    downloadLink.setAttribute('download', filename);
    document.body.appendChild(downloadLink);
    downloadLink.click();
    downloadLink.remove();
  }

  public getFilename(headers: HttpHeaders): string {
    const contentDisposition = headers.get('content-disposition');
    let filename = '';
    if (contentDisposition) {
      const part = contentDisposition
        .split(';')
        .find((item) => item && item.trim().startsWith('filename'));
      if (part) {
        filename = part
          .split('=')[1]
          .trim()
          .replace(/^"(.+?)"$/, '$1');
      }
    }
    return filename;
  }
}
