import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {AbstractApiService} from '@core/abstract-classes/abstract-api-service';
import {AbstractList} from '@core/abstract-classes/abstract-list';
import {ApiListResponse} from '@core/models/api-list-response';

@UntilDestroy()
@Component({template: ''})
export abstract class AbstractRequestedListComponent<T, N> extends AbstractList<T, N> {
  protected formSub = false;

  public baseFileName = 'list';
  public baseFileExtention = 'json';

  protected constructor(public fb: FormBuilder, public service: AbstractApiService) {
    super(fb, service);
  }

  public fetchList(): void {
    if (this.form.invalid) {
      return this.form.markAllAsTouched();
    }

    this.service
      .reqPost<ApiListResponse<T>>(this.tableFilter, this.requestOption)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.tableData = res.data;
        this.total = res.meta.total;

        if (!this.formSub) {
          this.formSub = true;
          this.filterList();
        }
      });
  }

  public downloadList(): void {
    if (this.form.invalid) {
      return this.form.markAllAsTouched();
    }

    this.service
      .reqGetFile(this.form.value, this.requestOption)
      .pipe(untilDestroyed(this))
      .subscribe((resp) => {
        const fileName =
          this.service.getFilename(resp.headers) ||
          `${this.baseFileName}.${this.baseFileExtention}`;
        if (resp?.body instanceof Blob) {
          this.service.downloadFile(resp.body, fileName);
        }
      });
  }
}
