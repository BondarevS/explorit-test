export const cleanFilter = (filter: {[param: string]: any}): {[param: string]: any} => {
  return Object.entries(filter).reduce((acc, [key, value]) => {
    if (value !== null && value !== '' && value !== undefined) {
      return {...acc, [key]: value};
    }
    return acc;
  }, {});
};
