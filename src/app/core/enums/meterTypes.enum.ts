export enum MeterTypes {
  WATER,
  ELECTRICITY,
  CAS,
  SANITATION
}
