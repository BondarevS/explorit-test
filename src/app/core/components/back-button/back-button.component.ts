import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.scss']
})
export class BackButtonComponent {
  @Input() backUrl!: string;
  @Input() text = 'Back';

  constructor(private router: Router, private location: Location) {}

  public back() {
    if (this.backUrl) {
      this.router.navigate([this.backUrl]);
      return;
    }

    this.location.back();
  }
}
