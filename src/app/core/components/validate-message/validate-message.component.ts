import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-validate-message',
  templateUrl: './validate-message.component.html',
  styleUrls: ['./validate-message.component.scss']
})
export class ValidateMessageComponent {
  @Input() field: AbstractControl | null = null;

  @Input() pattern!: string;

  public validatorMessages(): string[] {
    if (!this.field?.errors) {
      return [];
    }

    const errors: string[] = [];

    Object.keys(this.field.errors).forEach((error: string) => {
      switch (error) {
        case 'required':
          errors.push('Field is required');
          break;
        case 'maxlength':
          errors.push(`Max length is ${this.field?.errors?.maxlength?.requiredLength} characters`);
          break;
        case 'min':
          errors.push(`Min length is ${this.field?.errors?.maxlength?.requiredLength} characters`);
          break;
        case 'email':
          errors.push('Invalid email address');
          break;
      }
    });

    return errors;
  }
}
