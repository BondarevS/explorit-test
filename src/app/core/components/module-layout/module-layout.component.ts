import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-module-layout',
  templateUrl: './module-layout.component.html',
  styleUrls: ['./module-layout.component.scss']
})
export class ModuleLayoutComponent {
  public greyBackground = false;
  constructor(private router: Router) {
    this.greyBackground = this.router.url.includes('dashboard');
  }
}
