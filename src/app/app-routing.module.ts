import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@core/guards/auth.guard';
import {AppGuard} from '@core/guards/app.guard';
import {AppLayoutComponent} from '@core/components/app-layout/app-layout.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AppGuard],
    component: AppLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'leases'
      },
      {
        path: 'leases',
        loadChildren: () => import('./leases/leases.module').then((m) => m.LeasesModule)
      }
    ]
  },
  {
    path: 'auth',
    canActivate: [AuthGuard],
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule)
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
