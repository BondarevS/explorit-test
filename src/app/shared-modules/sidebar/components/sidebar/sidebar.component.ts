import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {NavigationLink} from '@core/models/navigation-link';

@UntilDestroy()
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  showWizard = false;

  links!: NavigationLink[];

  openedModule!: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'wand',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/wand.svg')
    );
  }

  ngOnInit(): void {
    this.route.data.pipe(untilDestroyed(this)).subscribe((data) => {
      this.links = data?.links;
      this.showWizard = data?.showWizard;
    });
    this.openedModule = this.router.url.substring(1).split('/')[1];
  }

  openModule(e: Event, path: string): void {
    e.preventDefault();
    this.openedModule = this.openedModule === path ? '' : path;
  }
}
