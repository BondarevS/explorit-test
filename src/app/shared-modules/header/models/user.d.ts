export interface User {
  // Represents any person that logs in as an employee or Member of an Estate Agency/Company
  ngStatus: string; // basic status after update - possible values ['OK','CHANGED','UPDATED','FAIL','ERROR']
  ngStatusMessage: string; // user-friendly descripiton of ngStatus
  ID: number;
  Username: string; // varchar(50) - username for this User
  Email: string; // varchar(255) - email address
  Status: string; // varchar(10) - either {'ACTIVE','SUSPENDED'}
  Readonly: boolean; // bit - User allowed read-only access only
  AgentAdminID: string; // varchar(10) - When this user is used for an Agent (AGTxxxxx) (optional)
  LoggedIn: boolean;
  loginResult: string;
  userType: string;
  loginMessage: String;
  error: string;
  errorMsg: string;
  CompanyID: number;
}
