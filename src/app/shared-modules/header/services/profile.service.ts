import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public $profile = new BehaviorSubject<User | null>(null);

  constructor() {}
}
