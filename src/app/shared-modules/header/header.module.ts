import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@material/material.module';
import {HeaderComponent} from './components/header/header.component';
import {CompaniesComponent} from './components/companies/companies.component';
import {ProfileComponent} from './components/profile/profile.component';

@NgModule({
  declarations: [HeaderComponent, CompaniesComponent, ProfileComponent],
  imports: [CommonModule, RouterModule, MaterialModule],
  exports: [HeaderComponent]
})
export class HeaderModule {}
