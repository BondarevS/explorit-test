import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {ProfileService} from '../../services/profile.service';
import {CoreService} from '@core/services/core.service';
import {User} from '../../models/user';
import {NavigationLink} from '@core/models/navigation-link';

@UntilDestroy()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  readonly actions: NavigationLink[] = [
    {
      path: '',
      title: 'My Account',
      iconClass: 'home'
    },
    {
      path: '',
      title: 'Company Setup',
      iconClass: 'build'
    },
    {
      path: '',
      title: 'Settings',
      iconClass: 'settings'
    },
    {
      path: '',
      title: 'Help & Support',
      iconClass: 'error'
    }
  ];

  public profile: User | null = null;

  constructor(
    private router: Router,
    private profileService: ProfileService,
    private coreService: CoreService
  ) {}

  ngOnInit(): void {
    this.profileService.$profile
      .pipe(
        filter((profile) => !!profile),
        untilDestroyed(this)
      )
      .subscribe((res) => (this.profile = res));
  }

  logout(): void {
    this.coreService.logout();
  }
}
