import {Component} from '@angular/core';
import {NavigationLink} from '@core/models/navigation-link';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  readonly links: NavigationLink[] = [
    {
      path: 'dashboard',
      title: 'dashboard',
      iconClass: 'home'
    },
    {
      path: 'leases',
      title: 'leases',
      iconClass: 'vpn_key'
    },
    {
      path: 'entities',
      title: 'entities',
      iconClass: 'filter_none'
    },
    {
      path: 'processing',
      title: 'processing',
      iconClass: 'cached'
    },
    {
      path: 'reports',
      title: 'reports',
      iconClass: 'description'
    }
  ];
}
